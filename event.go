package main

import (
  "time"
)

type Event struct {
  CreatedAt time.Time `json:"created_at"`
  UpdatedAt time.Time `json:"updated_at"`
  EventName string    `json:"event_name"`
  Name string    `json:"name"`
  OwnerEmail string    `json:"owner_email"`
  OwnerName string    `json:"owner_name"`
  Path     string    `json:"path"`
  ProjectId   int    `json:"project_id"`
  ProjectName   string    `json:"project_name,omitempty"`
  ProjectPath   string    `json:"project_path,omitempty"`
  ProjectVisibility   string    `json:"project_visibility,omitempty"`
  PathWithNamespace   string    `json:"path_with_namespace,omitempty"`
  OldPathWithNamespace   string    `json:"old_path_with_namespace,omitempty"`
  ProjectPathWithNamespace   string    `json:"project_path_with_namespace,omitempty"`
  UserId   int    `json:"user_id,omitempty"`
  Username   string    `json:"username,omitempty"`
  UserName   string    `json:"user_name,omitempty"`
  UserEmail   string    `json:"user_username,omitempty"`
  UserUsername   string    `json:"user_username,omitempty"`
  GroupId   int    `json:"group_id,omitempty"`
  GroupName   string    `json:"group_name,omitempty"`
  GroupPath   string    `json:"group_path,omitempty"`
  GroupAccess   string    `json:"group_access,omitempty"`
}
