package main

import (
  "testing"
)

func TestStringSet(t *testing.T) {
  ss := NewStringSet("test", "data")
  if !ss.Contains("data") {
    t.Fatal("StringSet Contains() does not find item in set")
  }
  if ss.Contains("missing") {
    t.Fatal("StringSet Contains() returns true for item not in set")
  }
}
