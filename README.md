# hookware

Easy GitLab hook listeners

## Example

```
import (
  "log"
  "net/http"
  "gitlab.com/gitlib/hookware"  
)

func main() {
  events := make(chan map[string]interface{})

  go func() {
    for {
      e := <-events
      log.Printf("Received event: %+v\n", e)
    }
  }()

  http.HandleFunc("/", hookware.HandleHook(events, hookware.Context{
    Events: []string{"System Hook", "Push Hook"},
    Tokens: []string{"secret-token"},
  }))
	http.ListenAndServe(":8080", nil)
}
```
