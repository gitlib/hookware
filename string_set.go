package main

type StringSet map[string]struct{}

func NewStringSet(values ...string) StringSet {
  var ss StringSet = make(map[string]struct{})
  for _, value := range values {
    ss.add(value)
  }
  return ss
}

func (ss StringSet) add(value string) {
  ss[value] = struct{}{}
}

func (ss StringSet) Contains(value string) bool {
  _, ok := ss[value]
  return ok
}
